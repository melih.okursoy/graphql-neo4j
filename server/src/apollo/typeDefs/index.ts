const typeDefs = `
    enum Role {
    ADMIN
    USER
  }

    type User {
    id: ID! @unique
    name: String 
    email: String! @unique
    password: String!
    phone: String @unique
    role: Role
    following: [User] @relation(name: "FOLLOWS", direction: OUT)
    followers: [User] @relation(name: "FOLLOWS", direction: IN)
    room: Room @relation(name: "IN_ROOM", direction: OUT)
  }
  
  type Room {
    id: ID! @unique
    title: String
    description: String
    timestamp: Int
    members: [User] @relation(name: "IN_ROOM", direction: IN)
  }

  type Mutation {
    signup(name: String!, email: String!, password: String!): User
    login(email: String!, password: String!): String
    logout:String
    me(token: String!): User
  }
  `;

export default typeDefs;