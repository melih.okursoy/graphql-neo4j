import neo4j from "neo4j-driver";

const user = process.env.NEO4J_ADMIN_USER;
const pass = process.env.NEO4J_ADMIN_SECRET;

const driver = neo4j.driver(
    process.env.NEO4J_ENDPOINT,
    neo4j.auth.basic(user, pass)
);

export const getDriver = () => driver;
